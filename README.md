This project was originally started from the Angular JS Tutorial

https://github.com/angular/angular-phonecat.git


To run this project, just get in 'angular-pms' directory and run 'npm start'

This is a patient management system where a doctor can create, view, update and delete patients. Doctors can add notes to patient as well.
Doctor can also search for patients by name and sort them by their last name and date of birth

The project is structured based on the features.

app/                     --> all the source code of the app (along with unit tests)
  bower_components/...   --> 3rd party JS/CSS libraries, including Angular and jQuery
  app.config.js          --> app-wide configuration of Angular services
  app.css                --> default stylesheet
  app.module.js          --> the main app module
  index.html             --> app layout file (the main HTML template file of the app)

  patient/
    patients.json - List of patients

  patient-add/
    - Add patient feature

  patient-detail/
    - View patient feature

  patient-list/
    - Show list of patient and filters

  patient-update/
    - update patient information

  resources/
    patient.js - service used by other modules
