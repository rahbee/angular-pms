// Configure the $route service for this application
angular.module('pmsApp').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/patients', {
                template: '<patient-list></patient-list>'
            })
            .when('/patients/new', {
                template: '<patient-add></patient-add>'
            })
            .when('/patients/:patientId', {
                template: '<patient-detail></patient-detail>'
            }).when('/patients/edit/:patientId', {
            template: '<patient-update></patient-update>'
        })
            .otherwise('/patients');
    }
]);