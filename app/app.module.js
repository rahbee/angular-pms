// pmsApp = Patient Management System Application

angular.module('pmsApp', [
    // Modules it depends on
    'ngRoute',
    'patientList',
    'patientDetail',
    'patientAdd',
    'patientUpdate'
]);