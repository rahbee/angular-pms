// Add patient

angular.module('patientAdd').component('patientAdd', {
    templateUrl: 'patient-add/patient-add.template.html',
    controller: ["patientResource", function patientAddController($patients) {
        this.patient = {};
        this.patient.notes = [];
        this.isSuccess = false;
        var self = this;

        this.submitForm = function (isValid) {
            return isValid;
        }

        this.createPatient = function(){
            var p = angular.copy(self.patient);
            p.dateOfBirth = p.dateOfBirth.toJSON();
            $patients.add(p)
                .then(function(){
                    self.isSuccess = true;
                })
        };
    }]
});