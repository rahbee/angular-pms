// Show patient details and add, delete notes

angular.module('patientDetail').component('patientDetail', {
    templateUrl: 'patient-detail/patient-detail.template.html',
    controller: ["patientResource",'$routeParams', function patientDetailController($patients, $routeParams) {
        this.newNote = "";
        var self = this;
        var data = '';
        
        this.deletePatient = function(){
            $patients.remove($routeParams.patientId)
                .then(function () {
                    
                });
        };

        this.addNote = function(){
            var note = {"note": self.newNote};
            $patients.addNote(self.patient.id, note)
            .then(function(n){
                //self.patient.notes.push(n);
            });
        };
        this.removeNote = function (d) {
            $patients.removeNote($routeParams.patientId, d).then(function(n){
            });

        }

        $patients.one($routeParams.patientId)
        .then(function(p){
            self.patient = p;
        });
    }]
});
