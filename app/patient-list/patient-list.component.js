// Show list of patients, search and sort patients, delete patients

angular.module('patientList').component('patientList', {
    templateUrl: 'patient-list/patient-list.template.html',
    controller: ['patientResource', '$filter', function patientListController($patients, $filter) {
        var self = this;
        self.orderProp = 'lastName';

        this.getFilteredAndSortedPatients = function(){
            if(typeof self.patients === "undefined" || !self.patients.length)
                return self.patients;

            var list = $filter("filter")(self.patients, function(item){
                if(!self.queryName)
                    return true;
                var search = self.queryName.toLowerCase();
                return item.firstName.toLowerCase().indexOf(search) >= 0 || item.lastName.toLowerCase().indexOf(search) >= 0; 
            });

            list = $filter("orderBy")(list, self.orderProp, self.orderProp === "dateOfBirth");

            if(self.dobFrom && self.dobTo) {
                list = $filter("filter")(list, function(item){
                    var dob = new Date(item.dateOfBirth);
                    return self.dobFrom <= dob && dob <= self.dobTo;
                })
            }

            return list;
        };

        this.deletePatientById = function (d) {
            $patients.remove(d);
        };

        $patients.all()
        .then(function(data){
            self.patients = data;
        });
    }]
});
