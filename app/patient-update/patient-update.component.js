// Update patient information
angular.module('patientUpdate').component('patientUpdate', {
    templateUrl: 'patient-update/patient-update.template.html',
    controller: ["patientResource", '$routeParams', function patientUpdateController($patients, $routeParams) {
        this.patient = {};
        this.patient.notes = [];
        this.isSuccess = false;
        var self = this;

        this.updatePatient = function(){
            $patients.update(self.patient)
            .then(function(){
                self.isSuccess = true;
            })
        };

        $patients.one($routeParams.patientId)
            .then(function(p){
                self.patient = p;
            });
    }]
});