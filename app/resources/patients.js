// Service used by other modules
angular.module("patientResource", [])
    .service("patientResource", [
        "$http", "$q",
        function ($http, $q) {
            var store = void(0);
            var nextPatientId = 4;
            var nextNoteId = 10;

            var ensureStorage = function () {
                if (typeof store !== "undefined")
                    return $q.when({success: true});

                return $http.get('patients/patients.json')
                    .then(function (resp) {
                        store = resp.data;
                        return {success: true};
                    });
            };


            var findIndexOfPatient = function (id) {
                var index = -1;
                store.forEach(function (item, i) {
                    if (item.id == id)
                        index = i;
                });

                return index;
            };

            var findIndexOfNote = function (id) {
                var index = -1;
                store.forEach(function (item, i) {
                    var notes = item.notes;
                    notes.forEach(function(note, j){
                        if (note.id == id)
                            index = j;
                    });
                });

                return index;
            }

            return {
                all: function () {
                    return ensureStorage()
                        .then(function () {
                            return store;
                        });
                },
                one: function (id) {
                    return ensureStorage()
                        .then(function () {
                            var filtered = store.filter(function (item) {
                                return item.id == id;
                            });

                            if (filtered.length > 0)
                                return filtered[0];

                            return void(0);
                        });
                },
                add: function (patient) {
                    patient.id = nextPatientId++;
                    return ensureStorage()
                        .then(function () {
                            store.push(patient);
                        });
                },
                remove: function (id) {
                    // console.log(id);
                    return ensureStorage()
                        .then(function () {
                            var index = findIndexOfPatient(id);
                            if (index < 0)
                                return false;

                            store.splice(index, 1);
                            return true;
                        });
                },
                update: function (patient) {
                    return ensureStorage()
                        .then(function () {
                            if (typeof patient.id === "undefined")
                                return false;

                            var index = findIndexOfPatient(patient.id);
                            if (index < 0)
                                return false;

                            store.splice(index, 1);
                            store.push(patient);
                            return true;
                        })
                },
                addNote: function (patientId, note) {
                    return ensureStorage()
                        .then(function () {
                            var index = findIndexOfPatient(patientId);

                            if (index < 0)
                                return false;

                            note.id = nextNoteId++;
                            // console.log(store[index]);
                            store[index].notes.push(note);
                            return note;
                        })
                },
                removeNote: function (patientId, noteId) {
                    return ensureStorage()
                        .then(function () {
                            var indexNote = findIndexOfNote(noteId);
                            var indexPatient = findIndexOfPatient(patientId);
                            if (indexNote < 0 || indexPatient < 0)
                                return false;

                            store[indexPatient].notes.splice(indexNote, 1);
                            return true;
                        })
                },
                updateNote: function (patientId, note) {
                    return ensureStorage()
                        .then(function () {
                            var indexNote = findIndexOfNote(note.id);
                            var indexPatient = findIndexOfPatient(patientId);
                            if (indexNote < 0 || indexPatient < 0)
                                return false;

                            note.id = nextNoteId++;
                            store[indexPatient].notes.splice(indexNote, 1);
                            store[indexPatient].notes.push(note);
                            return true;
                        })
                }
            }
        }
    ])